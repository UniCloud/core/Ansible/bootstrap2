newpool:
  zpool.present:
    - config:
        import: false
        force: true
    - properties:
        comment: salty storage pool
    - layout:
        - mirror:
          - /dev/sdb
          - /dev/sdc
