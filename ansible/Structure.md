# Best Practices
https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html

quote: 'This layout gives you more flexibility for larger environments, as well as a total separation of inventory variables between different environments.'

quote: 'Use Dynamic Inventory With Clouds
If you are using a cloud provider, you should not be managing your inventory in a static file. See Working with dynamic inventory.'

## Alternative Directory Layout
[Learned from:](https://github.com/ansible/ansible/issues/46096)
